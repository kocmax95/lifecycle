import React, {Component} from 'react';
import Loop from './Loop'
class App extends Component {

  state = {
    tabName: [
      {
        id: 1,
        name: "Maks",
        contact: 665176024,
      },
      {
        id: 2,
        name: "Edyta",
        contact: 535354178,
      },
      {
        id: 3,
        name: "Dawid",
        contact: 666555551,
      },
      {
        id: 4,
        name: "Bartek",
        contact: 548931622,
      }
    ],
    value: "",
    active: false,
  }

  handleInput = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  handleButton = () => {
    console.log(`kliknięty!!`);

    if(this.state.active){
      console.log(`true`)
    } else {
      // this.idInterval = setInterval(() => this.addSecond(), 1000)
      console.log(`false`)
    }

    this.setState({
      active: !this.state.active
    })
  }

  render() {


  return (
    <>
      <div>Hallo</div>
      <Loop value={this.state.value} onChange={this.handleInput} />
      {/* <input value={this.state.value} onChange={this.handleInput} type="text"/> */}
      <button onClick={this.handleButton}>Klik</button>

  <p>{this.state.active}? <h1>{this.state.value}</h1>:{null}</p>
    </>
  );
  }
}

export default App;
